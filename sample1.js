function hex_cell(parent,r,x,y)
{
 coord=[];
 var dang=Math.PI*2/6;
 for(i=0;i<6;++i)
   {
     coord.push([r*Math.sin(dang*i)+x,r*Math.cos(dang*i)+y].join(","));
   }
 coord = coord.join(" ");
 text_node=parent
          .append("polygon")
          .attr("fill","yellow")
          .attr("stroke","blue")
          .attr("points",coord);     this.text_node=parent.append("text")
         .attr("x", x)
         .attr("y", y)
         .attr("text-anchor","middle")
         .attr("alignment-baseline","middle");
  this.text=function(t){this.text_node.text(t);};
}

function FKoord(iy,ix,up_dn)
{
  var h2=0.8660254037844386;//Math.sqrt(3)/2;
      if(param.up_dn)
      {
        y = -iy*h2;
        x = ix-(iy)*0.5;
      }
      else
      {
        y = iy*h2;
        x = ix-iy*0.5;
      }
  return [y,x];

}

function color_hex_kart(param)
{
  var ix,iy,n,x,y;
  var elms;
  var xmin, ymin, xmax, ymax;
  
  if(!("up_dn" in param))
  {
    param.up_dn = 0;
  }
  
  n=param.row_start.length;
  var h2=Math.sqrt(3)/2;
  ymin=-1;
  ymax= h2 * n;
  xmin=1e8;
  xmax=-1e8;
  var yx=[];
  for(iy=0;iy<n;++iy)
  {
    yx = FKoord(iy,param.row_start[iy]);
    xmin=Math.min(xmin,yx[1]);
    yx = FKoord(iy,param.row_end[iy]-1);
    xmax=Math.max(xmax,yx[1]);
  }
  circ = param.parent.select("circle");
  var md=Math.max(xmax-xmin,ymax-ymin)/2;
  var cx_kart=(xmax+xmin)/2;
  var cy_kart=(ymax+ymin)/2;
  var cx=parseFloat(circ.attr("cx"));
  var cy=parseFloat(circ.attr("cy"));
  var r=parseFloat(circ.attr("r"));
  var scale_x= d3.scale.linear().domain([cx_kart-md,cx_kart+md]).range([cx-r,cx+r]);
  var scale_y= d3.scale.linear().domain([cy_kart-md,cy_kart+md]).range([cy-r,cy+r]);
//  console.log((cy+r)+" "+(cy-r));
  var h = scale_x(1./h2)-scale_x(0.);
  for(iy=0;iy<n;++iy)
  {
    for(ix=param.row_start[iy];ix<param.row_end[iy];++ix)
    {
      yx = FKoord(iy,ix);
          hs=new hex_cell(param.parent,h/2,scale_x(yx[1]),scale_y(yx[0]));
//hs.text("a");
//
      this.bg=param.parent.selectAll("polygone");
      this.txt=param.parent.selectAll("text");
      //console.log(scale_x(yx[1]),scale_y(yx[0]));
    }
  }
}


parent=d3.select("#panel");

a= new hex_cell(parent,30,50,50);
a.text("asdasd");
var row_start = [ 1,  0,  0,  0,  0,  0,  0,  1,  1,  2,  3,  4,  5,  6,  8];
var row_end = [ 7,  9, 10, 11, 12, 13, 14, 14, 15, 15, 15, 15, 15, 15, 14];
ckl= d3.select("#panel");

var param={"parent":ckl,row_start:row_start,"row_end":row_end,up_dn:0};
var a= new color_hex_kart(param);
var scale_x= d3.scale.linear().domain([-1,1]).range([0,1]);    