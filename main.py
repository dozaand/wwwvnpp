from bottle import get, run, route, static_file
from bottle.ext.websocket import GeventWebSocketServer
#from bottle.ext.websocket import websocket
from bottle import mako_template as template
from bottle import mako_view as view
import datetime
import json
import time
import gevent
from gevent import Greenlet

from bottle import request, Bottle, abort
import numpy as np

app = Bottle()

@app.route('<path:path>')
def static(path):
#    return static_file(path, root='C:/PROJECTS/virtualnpp/src/web_socket/static')
    return static_file(path, root='./static')

class TModel(Greenlet):
    def __init__(self, ws):
        Greenlet.__init__(self)
        self.ws=ws
        self.ok=1
        self.step=2000000000
        self.x=np.linspace(0,5,100)

    def _run(self):
        while self.ok:
            if self.step:
                t=unicode(datetime.datetime.now())
                self.x+=0.1
                y=np.sin(self.x)
#            print "step",t
                self.ws.send(json.dumps({"t":t,"y":y.tolist(),"x":self.x.tolist()}))
                if self.step:
                    self.step-=1
            gevent.sleep(0.05)

@app.get('/websocket')
def handle_websocket():
    ws = request.environ.get('wsgi.websocket')
    g = TModel(ws)
    ok=True
    #mod = Tmodel()
    g.start()
    while ok:
        # print " + msg",
        msg = json.loads(ws.receive())
        print "recived:",msg
#        print " ++ msg",
        if msg["cmd"]=="stop":
            g.ok=0
            break
        elif msg["cmd"]=="on_pause":
            g.step=0
        elif msg["cmd"]=="on_play":
            g.step=2000000000
        elif msg["cmd"]=="on_step":
            g.step=1

    g.join()
        #on_model_step(ws)
#        # if msg["cmd"]=="step":
        #     mod.step()
        # elif msg["cmd"]=="run":
        # elif msg["cmd"]=="stop":
        #     del mod
        #     mod = Tmodel()
        # elif msg["cmd"]=="pause":
#            ws.send(u"aaa "+t)

@app.get('/hello')
@view("d.mako")
def hello():
#    filecont=open("static/imj/drawing_sample.svg","rb").read()
    filecont=open("static/imj/main_format.svg","rb").read()
    return {"name" : "Nmae","filecont":filecont}

#app.run(host='localhost', port=8080, debug=True)
app.run(host='localhost', port=8080, server=GeventWebSocketServer, debug=True)

#p.selectAll("text").data([1,2,3,1,2,3]).enter().attr("x",function(d,i){20*i+40}).attr("y",40).text(d)