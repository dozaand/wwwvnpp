<!doctype html>
<head>
<meta charset="utf-8" />
<script type="text/javascript" src="brython.js"></script>
<script type="text/javascript" src="d3.min.js"></script>
<script>
function coord_pairs(v,r,x,y)
{
 coord=[];
 for(i=0;i<6;++i)
 {
   coord.push([r*v[i][0]+x,r*v[i][1]+y].join(","));
 }
 return coord.join(" ");
}
</script>
<script type="text/python">
from browser import document as doc
from browser import alert
from browser import svg
from browser import websocket
from browser import timer
from browser import window
import math
import json
from browser import html

btn_on_play = doc["on_play"]
btn_on_step = doc["on_step"]
btn_on_state_save = doc["on_state_save"]
btn_on_state_load = doc["on_state_load"]
out_text=doc["out_text"]
state_name = doc["state_name"]

runing = 1;

def on_play(evt):
    global runing,btn_on_play
    if runing:
        btn_on_play.text = "Play"
        runing = 0
        send(cmd="on_pause")
    else:
        btn_on_play.text = "Stop"
        runing = 1
        send(cmd="on_play")

def on_step(evt):
    btn_on_play.text = "Play"
    runing=0
    send(cmd="on_step")

def on_state_save(evt):
    send(cmd="save",nm=state_name.value)

def on_state_load(evt):
    send(cmd="load",nm=state_name.value)

def bindbtn():
    btn_on_play.bind('click',on_play)
    btn_on_step.bind('click',on_step)
    btn_on_state_save.bind('click',on_state_save)
    btn_on_state_load.bind('click',on_state_load)

def on_open(evt):
    on_play(None)

ws = None

def send(**arg):
    ws.send(json.dumps(arg))

def on_message(evt):
#    obj=json.loads(evt.data)
    obj=window.JSON.parse(evt.data)
    out_text.textContent = obj["t"]

def on_close(evt):
    send(cmd="close")

def _open_ws():
    if not __BRYTHON__.has_websocket:
        alert("WebSocket is not supported by your browser")
        return
    global ws
    ws = websocket.WebSocket("ws://localhost:8080/websocket")

    ws.bind('open',on_open)
    ws.bind('message',on_message)
    ws.bind('close',lambda evt:send(cmd="close"))
    bindbtn()

_open_ws()

class Thex_cell:
    dang = math.pi*2/6
    coord0 = [[math.sin(dang*i),math.cos(dang*i)]  for i in range(6)]
    def __init__(self,r,x,y):
#        coord = " ".join([",".join([str(r*self.coord0[i][0]+x),str(r*self.coord0[i][1]+y)])  for i in range(6)])
#        coord="0,0 20,0 20,20"
        coord=window.coord_pairs(self.coord0,r,x,y)
        self.hexagone = svg.polygon(fill="yellow", stroke="blue",points=coord)
        self.txt = svg.text("0",x=x,y=y,text_anchor="middle",alignment_baseline="middle")
    def render(self,parent):
        parent <= self.hexagone + self.txt

def FKoord(iy,ix,up_dn=0):
    """return [y,x]"""
    h2=0.8660254037844386 #;//Math.sqrt(3)/2;
    if up_dn:
        return [-iy*h2, ix-(iy)*0.5]
    else:
        return [iy*h2, ix-iy*0.5]

class Tscale:
    def __init__(self,x0,x1,y0,y1):
        delta=float(x1)-float(x0)
        self.a=(y1-y0)/delta
        self.b=(float(x1*y0)-float(x0*y1))/delta
    def __call__(self,x):
        if hasattr(x,"__iter__"):
            return [self.a*i+self.b for i in x]
        else:
            return self.a*x+self.b

class Tcolor_hex_kart:
    def __init__(self,parent, row_start = [ 1,  0,  0,  0,  0,  0,  0,  1,  1,  2,  3,  4,  5,  6,  8],
                      row_end = [ 7,  9, 10, 11, 12, 13, 14, 14, 15, 15, 15, 15, 15, 15, 14], up_dn=0):
        n=len(row_start)
        h2=math.sqrt(3)/2
        ymin=-1.
        ymax= h2 * n
        xmin=1e8
        xmax=-1e8
        xmin = min([FKoord(iy,row_start[iy],up_dn)[1] for iy in range(n)])
        xmax = max([FKoord(iy,row_end[iy]-1,up_dn)[1] for iy in range(n)])

        circ = parent.get(selector="circle")[0]
        md=max(xmax-xmin,ymax-ymin)/2
        cx_kart=(xmax+xmin)/2
        cy_kart=(ymax+ymin)/2
        cx = circ.cx.baseVal.value
        cy = circ.cy.baseVal.value
        r = circ.r.baseVal.value
        scale_x = Tscale(cx_kart-md,cx_kart+md,cx-r,cx+r)
        scale_y = Tscale(cy_kart-md,cy_kart+md,cy-r,cy+r)
        h = scale_x(1./h2)-scale_x(0.);
        yx = FKoord(7,7)
        for iy in range(n):
            for ix in range(row_start[iy],row_end[iy]):
                yx = FKoord(iy,ix)
                hx = Thex_cell(h/2, scale_x(yx[1]), scale_y(yx[0]))
                hx.render(parent)
#        self.bg=parent.get("polygone")
#        self.txt=parent.get("text");
#        print("lenobj",len(self.bg))

def bi_dict(key_list,node):
    """return dict of svg defined values and list of undefined"""
    dict_svg={}
    li=[]
    for i in key_list:
        if hasattr(node,i):
            v = node.__getattribute__(i)
            if v:
                dict_svg[i] = v
            else:
                li.append(i)
        else:
            li.append(i)
    return (dict_svg,li)

# функции должны возвращать наборы данных по событиям и конструктор объекта
# среди событий должно быть событие init
""" правило: 
в svg может
ключик отсутствовать - данные берем из конструктора по умолчанию
ключик присутствовать и быть пустым - данные берем из конструктора по умолчанию
ключик есть  - данные есть шаблон (судим по присутстствию $) - данные берем на каждом шаге или при инициализации с сервера
ключик есть  - данные не есть шаблон (судим по отсутствию $) - данные берем из svg
деление на данные инициализации и прочие события делаются в фабрике класса
"""

def color_hex_kart(node):
    print("color_hex_kart")
    evts={}
    init_keys = ["row_start","row_end","up_dn","format"]
    step_keys = ["var"]
    init_dict,init_ask = bi_dict(init_keys,node)
    print("init:",init_dict,init_ask)
    step_dict,step_ask = bi_dict(step_keys,node)
    print("step:",step_dict,step_ask)

def text_elm(node):
    print("text_elm")
#hk=Tcolor_hex_kart(doc["panel"])
hx = Thex_cell(30,50,50)
panel=doc.get(selector='svg')[0]
myobj =  doc.get(selector='[cls]')
construct_data = [] 
for i in myobj:
    try:
        f = globals()[i.cls]
        construct_data.append(f(i))
    except:
        alert("fail exec "+i.cls)
        raise
#    print(i.cls) 
#hx.render(panel)

</script>
<body onload="brython(debug_mode=1)">
 <h2 id=title>Виртуальная АЭС с ВВЭР</h2>
  <button id="on_play">Play</button>
  <button id="on_step">Step</button>
  <button id="on_state_save">Save State</button>
  <button id="on_state_load">Load State</button>
  <input id="state_name" type="text"/>
  <p id="out_text"></p>
  ${filecont}
<!--<svg width="600" height="600" style="border-style:solid;border-width:1;border-color:#000;">
  <g id="panel">
    <circle  cx="300" cy="300" r="200" fill="green"></circle>
    </g>
</svg>-->
</body>
</html>